<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

function pre() {
    echo '<pre>'.PHP_EOL.'<pre/>';
}

function arrayview($arr) {
    foreach ($arr[0] as $val) {

        echo  str_replace(' ',', ', $val);
    }
}

//Задача 1 на '.', символы:
//1) => Дана строка 'ahb acb aeb aeeb adcb axeb'.
// Напишите регулярку, которая найдет строки
// ahb, acb, aeb
// по шаблону: буква 'a', любой символ, буква 'b'.

echo 'TASK 1,1 = > ';
echo $string1 = 'ahb acb aeb aeeb adcb axeb';
pre();

preg_match_all('/(a.b+ )/', 'ahb acb aeb aeeb adcb axeb', $matches);// ahb, acb, aeb

arrayview($matches);

pre();

echo 'TASK 1,2 = > ';
//Дана строка 'aba aca aea abba adca abea'.
// Напишите регулярку, которая найдет строки
// abba adca abea
// по шаблону: буква 'a', 2 любых символа, буква 'a'.

$string2 = 'aba aca aea abba adca abea';
    preg_match_all('/(a..a)/','aba aca aea abba adca abea', $matches1);//abba adca abea

print_r($matches1[0]);

pre();

//Задача 2 на '+', '*', '?', ():
echo 'TASK 2,1 => ';

//1) = > Дана строка 'aa aba abba abbba abca abea'
//. Напишите регулярку, которая найдет строки aba, abba, abbba
//по шаблону: буква 'a', буква 'b' любое количество раз, буква 'a'.


$string3 = 'aa aba abba abbba abca abea';

preg_match_all('/(ab+a)/', 'aa aba abba abbba abca abea', $matches3);

print_r($matches3[0]);



pre();

echo 'TASK 2,2 => ';
//2) => Дана строка 'aa aba abba abbba abca abea'.
// Напишите регулярку, которая найдет строки aa,
// aba, abba, abbba по шаблону: буква 'a', буква 'b'
// любое количество раз (в том числе ниодного раза), буква 'a'.

$string3 = 'aa aba abba abbba abca abea';

preg_match_all('/(ab+&*a)/', 'aa aba abba abbba abca abea', $matches3);

print_r($matches3[0]);

//Задача 3 на экранировку:
echo 'TASK 3,1 =>';
pre();
//1) = >Дана строка 'a.a aba aea'.
// Напишите регулярку, которая найдет строку a.a, не захватив остальные.

echo preg_replace('/aba aea/', '','a.a aba aea');
pre();




echo 'TASK 3,2 => ';
//2)Дана строка '23 2+3 2++3 2+++3 345 567'.
// Напишите регулярку, которая найдет строки 2+3, 2++3, 2+++3,
// не захватив остальные (+ может быть любое количество).

$string = '23 2+3 2++3 2+++3 345 567';


preg_match_all('/2\++3/', $string, $matches4);

print_r($matches4[0]);

//preg_match_all('/\b2\+3\b \/','23 2+3 2++3 2+++3 345 567',$matches4);
//
//print_r($matches4[0]);
pre();

echo 'TASK 4 => ';
pre();
//Задача 4 на жадность:
//Дана строка 'aba accca azzza wwwwa'.
// Напишите регулярку, которая найдет все строки по краям которых стоят буквы 'a',
// и заменит каждую из них на '!'. Между буквами a может быть любой символ (кроме a).

$string6 = 'aba accca azzza wwwwa';
echo preg_replace('/a/', '!', $string6);
pre();

echo 'TASK 5,1 => ';
pre();

//Задача 5 на {}:
//1)Дана строка 'aa aba abba abbba abbbba abbbbba'.
// Напишите регулярку, которая найдет строки abba, abbba, abbbba и только их.

$string7 = 'aa aba abba abbba abbbba abbbbba';

preg_match_all('[\babba\sabbba\sabbbba\b] ', $string7, $matches5);
print_r($matches5[0]);

echo 'TASK 5,2 => ';
pre();
//2)Дана строка 'aa aba abba abbba abbbba abbbbba'.
// Напишите регулярку,
// 	которая найдет строки вида aba,
// в которых 'b' встречается менее 3-х раз (включительно).

$string8 = 'aa aba abba abbba abbbba abbbbba';

preg_match_all('/ab{1,2}a/', $string8, $matches8);
print_r($matches8);

pre();

echo 'TASK 6,1 => ';
pre();

//Задача 6 на \s, \S, \w, \W, \d, \D:
//1)Дана строка 'a1a a2a a3a a4a a5a aba aca'.
// Напишите регулярку, которая найдет строки, в которых по краям стоят буквы 'a', а между ними одна цифра.

$stringfornumber = 'a1a a2a a3a a4a a5a aba aca';

preg_match_all('/a[0-9]a/', $stringfornumber, $matches9);
print_r($matches9[0]);


echo 'TASK 6,2 => ';
pre();

//2)Дана строка 'ave a#a a2a a$a a4a a5a a-a aca'. Напишите регулярку, которая заменит все пробелы на '!'.

$stringofspase = 'ave a#a a2a a$a a4a a5a a-a aca';

echo preg_replace('/\s/', '!', $stringfornumber);
pre();

echo 'TASK 7,1 => ';
pre();
//Задача 7 на [], '^' - не, [a-zA-Z], кириллицу:
//1)Дана строка 'aba aea aca aza axa'.
// Напишите регулярку,
// которая найдет строки aba, aea, axa, не затронув остальных.

preg_match_all('/a[a-be-x]a/','aba aea aca aza axa', $matches10 );//aba, aea, axa
print_r($matches10);

echo 'TASK 7,2 => ';
pre();

//2)Напишите регулярку,
// которая найдет строки следующего вида:
// по краям стоят буквы 'a', а между ними - цифра от 3-х до 7-ми.

$strNum1 = 'a3a a7a e6a a1a s33c a4rta sddd';

preg_match_all('/a[3-7a-z]a/', $strNum1, $matches11);
print_r($matches11[0]);

echo 'TASK 8,1 => ';
pre();

//Задача 8 на [a-zA-Z] и квантификаторы:
//1)Дана строка 'aAXa aeffa aGha aza ax23a a3sSa'.
//Напишите регулярку, которая найдет строки следующего вида:
// по краям стоят буквы 'a', а между ними - маленькие латинские буквы, не затронув остальных.




echo 'TASK 8,2 => ';
pre();
//2)Дана строка 'ааа ббб ёёё ззз ййй ААА БББ ЁЁЁ ЗЗЗ ЙЙЙ'.
// Напишите регулярку, которая найдет все слова по шаблону:
// любая кириллическая буква любое количество раз.